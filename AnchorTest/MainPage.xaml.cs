﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Xamarin.Forms;

namespace AnchorTest
{
	// Learn more about making custom code visible in the Xamarin.Forms previewer
	// by visiting https://aka.ms/xamarinforms-previewer
	[DesignTimeVisible(false)]
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}

		void HandleButton (Object sender, EventArgs e)
		{
			rightBox.Scale = 0.5;

			wrongBox.HeightRequest = 160;
			wrongBox.Scale = 0.5;
		}
	}
}
